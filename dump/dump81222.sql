-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: resort_network
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `menu_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'ushqime'),(2,'pijet');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orar_operimi`
--

DROP TABLE IF EXISTS `orar_operimi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orar_operimi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orari` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orar_operimi`
--

LOCK TABLES `orar_operimi` WRITE;
/*!40000 ALTER TABLE `orar_operimi` DISABLE KEYS */;
INSERT INTO `orar_operimi` VALUES (1,'7-11'),(2,'11-11');
/*!40000 ALTER TABLE `orar_operimi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personeli`
--

DROP TABLE IF EXISTS `personeli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personeli` (
  `id` int NOT NULL AUTO_INCREMENT,
  `emer` varchar(45) NOT NULL,
  `mbiemer` varchar(45) NOT NULL,
  `mosha` int DEFAULT NULL,
  `gjinia` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `adresa` varchar(45) DEFAULT NULL,
  `paga` int DEFAULT NULL,
  `ditepushimi` varchar(45) DEFAULT NULL,
  `muaj` varchar(45) DEFAULT NULL,
  `turni_id` int NOT NULL,
  `pozicioni_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_personeli_turni1_idx` (`turni_id`),
  KEY `fk_personeli_pozicioni1_idx` (`pozicioni_id`),
  CONSTRAINT `fk_personeli_pozicioni1` FOREIGN KEY (`pozicioni_id`) REFERENCES `pozicioni` (`id`),
  CONSTRAINT `fk_personeli_turni1` FOREIGN KEY (`turni_id`) REFERENCES `turni` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personeli`
--

LOCK TABLES `personeli` WRITE;
/*!40000 ALTER TABLE `personeli` DISABLE KEYS */;
/*!40000 ALTER TABLE `personeli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pozicioni`
--

DROP TABLE IF EXISTS `pozicioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pozicioni` (
  `id` int NOT NULL,
  `profesioni` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pozicioni`
--

LOCK TABLES `pozicioni` WRITE;
/*!40000 ALTER TABLE `pozicioni` DISABLE KEYS */;
INSERT INTO `pozicioni` VALUES (1,'recepsion'),(2,'menaxher'),(3,'administrator'),(4,'sanitare'),(5,'roje');
/*!40000 ALTER TABLE `pozicioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restorant`
--

DROP TABLE IF EXISTS `restorant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restorant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `emer` varchar(45) NOT NULL,
  `codeFilial` varchar(45) NOT NULL,
  `nipt` varchar(45) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `qyteti` varchar(45) NOT NULL,
  `rruga` varchar(45) DEFAULT NULL,
  `tel_one` varchar(45) DEFAULT NULL,
  `tel_two` varchar(45) DEFAULT NULL,
  `tavolina_id` int NOT NULL,
  `tavoline_type_id` int NOT NULL,
  `menu_id` int NOT NULL,
  `orar_operimi_id` int NOT NULL,
  `restorant_type_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_restorant_tavolina_idx` (`tavolina_id`),
  KEY `fk_restorant_tavoline_type1_idx` (`tavoline_type_id`),
  KEY `fk_restorant_menu1_idx` (`menu_id`),
  KEY `fk_restorant_orar_operimi1_idx` (`orar_operimi_id`),
  KEY `fk_restorant_restorant_type1_idx` (`restorant_type_id`),
  CONSTRAINT `fk_restorant_menu1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`),
  CONSTRAINT `fk_restorant_orar_operimi1` FOREIGN KEY (`orar_operimi_id`) REFERENCES `orar_operimi` (`id`),
  CONSTRAINT `fk_restorant_restorant_type1` FOREIGN KEY (`restorant_type_id`) REFERENCES `restorant_type` (`id`),
  CONSTRAINT `fk_restorant_tavolina` FOREIGN KEY (`tavolina_id`) REFERENCES `tavolina` (`id`),
  CONSTRAINT `fk_restorant_tavoline_type1` FOREIGN KEY (`tavoline_type_id`) REFERENCES `tavoline_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restorant`
--

LOCK TABLES `restorant` WRITE;
/*!40000 ALTER TABLE `restorant` DISABLE KEYS */;
/*!40000 ALTER TABLE `restorant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restorant_type`
--

DROP TABLE IF EXISTS `restorant_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restorant_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restorant_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restorant_type`
--

LOCK TABLES `restorant_type` WRITE;
/*!40000 ALTER TABLE `restorant_type` DISABLE KEYS */;
INSERT INTO `restorant_type` VALUES (1,'restorant peshku'),(2,'tradicional'),(3,'piceri'),(4,'bar restorant piceri');
/*!40000 ALTER TABLE `restorant_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rezervime`
--

DROP TABLE IF EXISTS `rezervime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rezervime` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kodi` varchar(45) DEFAULT NULL,
  `klientemer` varchar(45) NOT NULL,
  `tel1` varchar(45) DEFAULT NULL,
  `kerkesa specifike` varchar(45) DEFAULT NULL,
  `tavolin_no` varchar(45) DEFAULT NULL,
  `orari` varchar(45) DEFAULT NULL,
  `restorant_id` int NOT NULL,
  `vakti_id` int NOT NULL,
  `personeli_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_rezervime_restorant1_idx` (`restorant_id`),
  KEY `fk_rezervime_vakti1_idx` (`vakti_id`),
  KEY `fk_rezervime_personeli1_idx` (`personeli_id`),
  CONSTRAINT `fk_rezervime_personeli1` FOREIGN KEY (`personeli_id`) REFERENCES `personeli` (`id`),
  CONSTRAINT `fk_rezervime_restorant1` FOREIGN KEY (`restorant_id`) REFERENCES `restorant` (`id`),
  CONSTRAINT `fk_rezervime_vakti1` FOREIGN KEY (`vakti_id`) REFERENCES `vakti` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rezervime`
--

LOCK TABLES `rezervime` WRITE;
/*!40000 ALTER TABLE `rezervime` DISABLE KEYS */;
/*!40000 ALTER TABLE `rezervime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rur`
--

DROP TABLE IF EXISTS `rur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rur` (
  `id` int NOT NULL AUTO_INCREMENT,
  `rezervime_id` int NOT NULL,
  `ushqimet_id` int NOT NULL,
  `restorant_id` int NOT NULL,
  `code_id` varchar(200) NOT NULL,
  `sasia` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_rur_rezervime1_idx` (`rezervime_id`),
  KEY `fk_rur_ushqimet1_idx` (`ushqimet_id`),
  KEY `fk_rur_restorant1_idx` (`restorant_id`),
  CONSTRAINT `fk_rur_restorant1` FOREIGN KEY (`restorant_id`) REFERENCES `restorant` (`id`),
  CONSTRAINT `fk_rur_rezervime1` FOREIGN KEY (`rezervime_id`) REFERENCES `rezervime` (`id`),
  CONSTRAINT `fk_rur_ushqimet1` FOREIGN KEY (`ushqimet_id`) REFERENCES `ushqimet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rur`
--

LOCK TABLES `rur` WRITE;
/*!40000 ALTER TABLE `rur` DISABLE KEYS */;
/*!40000 ALTER TABLE `rur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tavolina`
--

DROP TABLE IF EXISTS `tavolina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tavolina` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tavolin_no` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tavolina`
--

LOCK TABLES `tavolina` WRITE;
/*!40000 ALTER TABLE `tavolina` DISABLE KEYS */;
/*!40000 ALTER TABLE `tavolina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tavoline_type`
--

DROP TABLE IF EXISTS `tavoline_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tavoline_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tavoline_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tavoline_type`
--

LOCK TABLES `tavoline_type` WRITE;
/*!40000 ALTER TABLE `tavoline_type` DISABLE KEYS */;
INSERT INTO `tavoline_type` VALUES (1,'tavolin cift'),(2,'tavoline familjare 4'),(3,'tavoline familjare 6'),(4,'tavoline familjare 8');
/*!40000 ALTER TABLE `tavoline_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turni`
--

DROP TABLE IF EXISTS `turni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turni` (
  `id` int NOT NULL AUTO_INCREMENT,
  `turni` varchar(45) DEFAULT NULL,
  `orari` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turni`
--

LOCK TABLES `turni` WRITE;
/*!40000 ALTER TABLE `turni` DISABLE KEYS */;
INSERT INTO `turni` VALUES (1,'turni 1','9-15'),(2,'turni 2','15-21');
/*!40000 ALTER TABLE `turni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ushqimet`
--

DROP TABLE IF EXISTS `ushqimet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ushqimet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pjatat` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ushqimet`
--

LOCK TABLES `ushqimet` WRITE;
/*!40000 ALTER TABLE `ushqimet` DISABLE KEYS */;
/*!40000 ALTER TABLE `ushqimet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vakti`
--

DROP TABLE IF EXISTS `vakti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vakti` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vakti` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vakti`
--

LOCK TABLES `vakti` WRITE;
/*!40000 ALTER TABLE `vakti` DISABLE KEYS */;
INSERT INTO `vakti` VALUES (1,'mgjesi'),(2,'dreka'),(3,'darka');
/*!40000 ALTER TABLE `vakti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-08 17:33:55
