-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema resort_network
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `resort_network` ;

-- -----------------------------------------------------
-- Schema resort_network
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `resort_network` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `resort_network` ;

-- -----------------------------------------------------
-- Table `resort_network`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `menu_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`menu` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`orar_operimi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`orar_operimi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `orari` TIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`orar_operimi` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`restorant_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`restorant_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `restorant_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`restorant_type` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`tavolina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`tavolina` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tavolin_no` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`tavolina` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`tavoline_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`tavoline_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tavoline_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`tavoline_type` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`restorant`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`restorant` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `emer` VARCHAR(45) NOT NULL,
  `codeFilial` VARCHAR(45) NOT NULL,
  `nipt` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `qyteti` VARCHAR(45) NOT NULL,
  `rruga` VARCHAR(45) NULL DEFAULT NULL,
  `tel_one` VARCHAR(45) NULL DEFAULT NULL,
  `tel_two` VARCHAR(45) NULL DEFAULT NULL,
  `tavolina_id` INT NOT NULL,
  `tavoline_type_id` INT NOT NULL,
  `menu_id` INT NOT NULL,
  `orar_operimi_id` INT NOT NULL,
  `restorant_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_restorant_menu1`
    FOREIGN KEY (`menu_id`)
    REFERENCES `resort_network`.`menu` (`id`),
  CONSTRAINT `fk_restorant_orar_operimi1`
    FOREIGN KEY (`orar_operimi_id`)
    REFERENCES `resort_network`.`orar_operimi` (`id`),
  CONSTRAINT `fk_restorant_restorant_type1`
    FOREIGN KEY (`restorant_type_id`)
    REFERENCES `resort_network`.`restorant_type` (`id`),
  CONSTRAINT `fk_restorant_tavolina`
    FOREIGN KEY (`tavolina_id`)
    REFERENCES `resort_network`.`tavolina` (`id`),
  CONSTRAINT `fk_restorant_tavoline_type1`
    FOREIGN KEY (`tavoline_type_id`)
    REFERENCES `resort_network`.`tavoline_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`restorant` (`id` ASC) VISIBLE;

CREATE INDEX `fk_restorant_tavolina_idx` ON `resort_network`.`restorant` (`tavolina_id` ASC) VISIBLE;

CREATE INDEX `fk_restorant_tavoline_type1_idx` ON `resort_network`.`restorant` (`tavoline_type_id` ASC) VISIBLE;

CREATE INDEX `fk_restorant_menu1_idx` ON `resort_network`.`restorant` (`menu_id` ASC) VISIBLE;

CREATE INDEX `fk_restorant_orar_operimi1_idx` ON `resort_network`.`restorant` (`orar_operimi_id` ASC) VISIBLE;

CREATE INDEX `fk_restorant_restorant_type1_idx` ON `resort_network`.`restorant` (`restorant_type_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`ushqimet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`ushqimet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `pjatat` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`ushqimet` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`vakti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`vakti` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `vakti` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`vakti` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`turni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`turni` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `turni` VARCHAR(45) NULL,
  `orari` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`turni` (`id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`pozicioni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`pozicioni` (
  `id` INT NOT NULL,
  `profesioni` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `resort_network`.`personeli`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`personeli` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `emer` VARCHAR(45) NOT NULL,
  `mbiemer` VARCHAR(45) NOT NULL,
  `mosha` INT NULL,
  `gjinia` VARCHAR(45) NULL,
  `tel` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `adresa` VARCHAR(45) NULL,
  `paga` INT NULL,
  `ditepushimi` VARCHAR(45) NULL,
  `muaj` VARCHAR(45) NULL,
  `turni_id` INT NOT NULL,
  `pozicioni_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_personeli_turni1`
    FOREIGN KEY (`turni_id`)
    REFERENCES `resort_network`.`turni` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_personeli_pozicioni1`
    FOREIGN KEY (`pozicioni_id`)
    REFERENCES `resort_network`.`pozicioni` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`personeli` (`id` ASC) VISIBLE;

CREATE INDEX `fk_personeli_turni1_idx` ON `resort_network`.`personeli` (`turni_id` ASC) VISIBLE;

CREATE INDEX `fk_personeli_pozicioni1_idx` ON `resort_network`.`personeli` (`pozicioni_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`rezervime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`rezervime` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `kodi` VARCHAR(45) NULL,
  `klientemer` VARCHAR(45) NOT NULL,
  `tel1` VARCHAR(45) NULL,
  `kerkesa specifike` VARCHAR(45) NULL,
  `tavolin_no` VARCHAR(45) NULL,
  `orari` VARCHAR(45) NULL,
  `restorant_id` INT NOT NULL,
  `vakti_id` INT NOT NULL,
  `personeli_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_rezervime_restorant1`
    FOREIGN KEY (`restorant_id`)
    REFERENCES `resort_network`.`restorant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rezervime_vakti1`
    FOREIGN KEY (`vakti_id`)
    REFERENCES `resort_network`.`vakti` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rezervime_personeli1`
    FOREIGN KEY (`personeli_id`)
    REFERENCES `resort_network`.`personeli` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`rezervime` (`id` ASC) VISIBLE;

CREATE INDEX `fk_rezervime_restorant1_idx` ON `resort_network`.`rezervime` (`restorant_id` ASC) VISIBLE;

CREATE INDEX `fk_rezervime_vakti1_idx` ON `resort_network`.`rezervime` (`vakti_id` ASC) VISIBLE;

CREATE INDEX `fk_rezervime_personeli1_idx` ON `resort_network`.`rezervime` (`personeli_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `resort_network`.`rur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `resort_network`.`rur` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rezervime_id` INT NOT NULL,
  `ushqimet_id` INT NOT NULL,
  `restorant_id` INT NOT NULL,
  `code_id` VARCHAR(200) NOT NULL,
  `sasia` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_rur_rezervime1`
    FOREIGN KEY (`rezervime_id`)
    REFERENCES `resort_network`.`rezervime` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rur_ushqimet1`
    FOREIGN KEY (`ushqimet_id`)
    REFERENCES `resort_network`.`ushqimet` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rur_restorant1`
    FOREIGN KEY (`restorant_id`)
    REFERENCES `resort_network`.`restorant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `resort_network`.`rur` (`id` ASC) VISIBLE;

CREATE INDEX `fk_rur_rezervime1_idx` ON `resort_network`.`rur` (`rezervime_id` ASC) VISIBLE;

CREATE INDEX `fk_rur_ushqimet1_idx` ON `resort_network`.`rur` (`ushqimet_id` ASC) VISIBLE;

CREATE INDEX `fk_rur_restorant1_idx` ON `resort_network`.`rur` (`restorant_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
