package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class UshqimetDto {
    private int id;
    private String ushqimet;
}
