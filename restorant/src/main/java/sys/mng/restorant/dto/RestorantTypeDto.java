package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class RestorantTypeDto {
    public int id;
    public String restorantType;

}
