package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RestorantDto {
    public int id;
    public String emer;
    public String codeFilial;
    public String nipt;
    public String address;
    public String qyteti;
    public String rruga;
    public String telOne;
    public String telTwo;
    public TavolinaDto tavolinaDto;
    public TavolineTypeDto tavolineTypeDto;
    public MenuDto menuDto;
    public OrarOperimiDto orarOperimiDto;
    public RestorantTypeDto restorantTypeDto;

}
