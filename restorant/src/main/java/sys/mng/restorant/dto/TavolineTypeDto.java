package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class TavolineTypeDto {
    public int id;
    public String tavolineType;
}
