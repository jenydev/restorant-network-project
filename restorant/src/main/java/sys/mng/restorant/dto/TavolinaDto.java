package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class TavolinaDto {
    public int id;
    public String tavolinNo;
}
