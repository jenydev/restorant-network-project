package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TurniDto {
    private int id;
    private String turni;
    private String orari;
}
