package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class PersoneliDto {
    public int id;
    public String emer;
    public String mbiemer;
    public Integer mosha;
    public String gjinia;
    public String tel;
    public String email;
    public String adresa;
    public Integer paga;
    public String ditePushimi;
    public String muaj;
    public TurniDto turniDto;
    public  PozicioniDto pozicioniDto;

}
