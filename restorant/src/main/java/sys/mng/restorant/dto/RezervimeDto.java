package sys.mng.restorant.dto;

import lombok.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class RezervimeDto {
    public int id;
    public String kodi;
    public String KlientEmer;
    public String Tel1;
    public String kerkesaSpecifike;
    public TavolinaDto tavolinaDto;
    public String orari;
    public RestorantDto restorantDto;
    public VaktiDto vaktiDto;
    public PersoneliDto personeliDto;

}
