package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class OrarOperimiDto {
    public  int id;
    public String orari;
}
