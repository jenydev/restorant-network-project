package sys.mng.restorant.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RURDto {
    private int id;
    private RezervimeDto rezervimeDto;
    private UshqimetDto ushqimetDto;
    private RestorantDto restorantDto;
    private String codeId;
    private Integer sasia;
}
