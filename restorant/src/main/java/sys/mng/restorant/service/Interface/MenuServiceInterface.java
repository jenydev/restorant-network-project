package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.MenuDto;

import java.util.List;

public interface MenuServiceInterface {
    List<MenuDto> getAll();
    MenuDto getById(Integer id);
    MenuDto create(MenuDto body);
    MenuDto update(MenuDto body);
    MenuDto deleteById(Integer id);
}
