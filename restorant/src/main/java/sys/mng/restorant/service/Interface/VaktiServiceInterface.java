package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.VaktiDto;

import java.util.List;

public interface VaktiServiceInterface {
    List<VaktiDto> getAll();
    VaktiDto getbyId(Integer id);
    VaktiDto create(VaktiDto body);
    VaktiDto update(VaktiDto body);
    VaktiDto deletebyId(Integer id);
}
