package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.TavolineTypeDto;

import java.util.List;

public interface TavolineTypeServiceInterface {
    List<TavolineTypeDto> getAll();
    TavolineTypeDto getbyId(Integer id);
    TavolineTypeDto create(TavolineTypeDto body);
    TavolineTypeDto update(TavolineTypeDto body);
    TavolineTypeDto deletebyId(Integer id);
}
