package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.OrarOperimiDto;

import java.util.List;

public interface OrarOperimiServiceInterface {
    List<OrarOperimiDto> getAll();
    OrarOperimiDto getbyId(Integer id);
    OrarOperimiDto create(OrarOperimiDto body);
    OrarOperimiDto update(OrarOperimiDto body);
    OrarOperimiDto deletebyId(Integer id);

}
