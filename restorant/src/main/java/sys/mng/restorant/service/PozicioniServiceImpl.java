package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.PozicioniDto;
import sys.mng.restorant.mapper.Interface.PozicioniMapperInterface;
import sys.mng.restorant.repository.PozicioniRepository;
import sys.mng.restorant.service.Interface.PozicioniServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PozicioniServiceImpl implements PozicioniServiceInterface {
    @Autowired
    public PozicioniRepository repository;
    @Autowired
    public PozicioniMapperInterface mapper;
    @Override
    public List<PozicioniDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public PozicioniDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public PozicioniDto create(PozicioniDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public PozicioniDto update(PozicioniDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public PozicioniDto deletebyId(Integer id) {
        PozicioniDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
}
