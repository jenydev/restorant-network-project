package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.TavolinaDto;
import sys.mng.restorant.mapper.Interface.TavolinaMapperInterface;
import sys.mng.restorant.repository.TavolinaRepository;
import sys.mng.restorant.service.Interface.TavolinaServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TavolinaServiceImpl implements TavolinaServiceInterface {

    @Autowired
    public TavolinaRepository repository;
    @Autowired
    public TavolinaMapperInterface mapper;
    @Override
    public List<TavolinaDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public TavolinaDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public TavolinaDto create(TavolinaDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public TavolinaDto update(TavolinaDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public TavolinaDto deletebyId(Integer id) {
        TavolinaDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
}
