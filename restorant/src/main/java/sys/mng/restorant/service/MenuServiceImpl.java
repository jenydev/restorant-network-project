package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.MenuDto;
import sys.mng.restorant.mapper.Interface.MenuMapperInterface;
import sys.mng.restorant.repository.MenuRepository;
import sys.mng.restorant.service.Interface.MenuServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class MenuServiceImpl implements MenuServiceInterface {
    @Autowired
    public MenuRepository repository;
    @Autowired
    public MenuMapperInterface mapper;


    @Override
    public List<MenuDto> getAll() {
        return repository.findAll().stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public MenuDto getById(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public MenuDto create(MenuDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public MenuDto update(MenuDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public MenuDto deleteById(Integer id) {
        MenuDto delete = getById(id);
        repository.deleteById(id);
        return delete;    }


}
