package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.RestorantTypeDto;

import java.util.List;

public interface RestorantTypeServiceInterface {
    List<RestorantTypeDto> getAll();
    RestorantTypeDto getbyId(Integer id);
    RestorantTypeDto create(RestorantTypeDto body);
    RestorantTypeDto update(RestorantTypeDto body);
    RestorantTypeDto deletebyId(Integer id);
}
