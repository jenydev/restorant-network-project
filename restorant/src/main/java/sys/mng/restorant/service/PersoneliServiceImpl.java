package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.PersoneliDto;
import sys.mng.restorant.mapper.Interface.PersoneliMapperInterface;
import sys.mng.restorant.model.Personeli;
import sys.mng.restorant.repository.PersoneliRepository;
import sys.mng.restorant.service.Interface.PersoneliServiceInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor RequiredArgsConstructor
@Service
public class PersoneliServiceImpl implements PersoneliServiceInterface {

    @Autowired
    public PersoneliRepository repository;

    @Autowired
    public PersoneliMapperInterface mapper;


    //sort variable
    private static final String TURNI = "turni";
    private static final String TURNIDTO = "turniDto." + TURNI;

   private static final String EMER = "emer";
   private static final String MBIEMRI = "mbiemer";
   private static final String MOSHA = "mosha";
   private static final String GJITNIA = "gjinia";
    private static final String TEL = "tel";
    private static final String EMAIL = "email";
    private static final String ADRESA = "adresa";
    private static final String PAGA = "paga";
   private static final String DITEPUSHIMI = "ditePushimi";
   private static final String MUAJ = "muaj";
   private static final String TURNI = "turniDto.";
    private static final String POZICIONI = "pozicioniDto.";





    @Override
    public List<PersoneliDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public PersoneliDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public PersoneliDto create(PersoneliDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public PersoneliDto update(PersoneliDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public PersoneliDto deletebyId(Integer id) {
        PersoneliDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }


    @Override
    public List<PersoneliDto> getPersoneliByTurniId(Integer turniId){
        return  repository.selectPersoneliByTurniId(turniId)
                .stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Page<PersoneliDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending, String emer, String mbiemer, String mosha, String tel, String email, String adresa, String paga, String ditePushimi, String muaj) {
        return null;
    }

    @Override
    public Page<PersoneliDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending, String emer, String mbiemer,
                                     String mosha, String tel, String email, String adresa, String paga,
                                     String ditePushimi, String muaj, String turni, String pozicioni) {
        return null;
    }

    @Override
    public Page<PersoneliDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                                     String emer, String mbiemer, String mosha, String tel, String email, String adresa, String paga, String ditePushimi,
                                     String muaj, String turni, String pozicioni) {


        if (TURNI.equals(sort))
            sort = TURNIDTO;

        Sort sortingOption = isAscending
                ? Sort.by(sort).ascending()
                : Sort.by(sort).descending();

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sortingOption);


        List<Personeli> personeliList = repository.searchPersoneli( emer,  mbiemer,  mosha,  tel,  email,  adresa,  paga,  ditePushimi,
                 muaj,turni,  pozicioni, pageable);

        List<PersoneliDto> personeliDtos = new ArrayList<>();
        Integer count = repository.countPersoneli( emer,  mbiemer,  mosha,  tel,  email,  adresa,  paga,  ditePushimi, muaj, turni,  pozicioni);

        for (Personeli p : personeliList)
            personeliDtos.add(mapper.toDto(p));

        return new PageImpl<>(personeliDtos, pageable, count);
    }
}
