package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.VaktiDto;
import sys.mng.restorant.mapper.Interface.VaktiMapperInterface;
import sys.mng.restorant.repository.VaktiRepository;
import sys.mng.restorant.service.Interface.VaktiServiceInterface;

import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
public class VaktiServiceImpl implements VaktiServiceInterface {

    @Autowired
    public VaktiRepository repository;

    @Autowired
    public VaktiMapperInterface mapper;

    @Override
    public List<VaktiDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public VaktiDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public VaktiDto create(VaktiDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public VaktiDto update(VaktiDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public VaktiDto deletebyId(Integer id) {
         VaktiDto delete = getbyId(id);
         repository.deleteById(id);
        return delete;
    }
}
