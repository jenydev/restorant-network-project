package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.UshqimetDto;

import java.util.List;

public interface UshqimetServiceInterface {
    List<UshqimetDto> getAll();
    UshqimetDto getbyId(Integer id);
    UshqimetDto create(UshqimetDto body);
    UshqimetDto update(UshqimetDto body);
    UshqimetDto deletebyId(Integer ID);
}
