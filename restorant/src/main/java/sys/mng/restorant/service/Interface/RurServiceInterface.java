package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.RURDto;

import java.util.List;

public interface RurServiceInterface {
    List<RURDto> getAll();
    RURDto getbyId(Integer id);
    RURDto create(RURDto body);
    RURDto update(RURDto body);
    RURDto deletebyId(Integer id);
}
