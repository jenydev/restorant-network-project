package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.RestorantTypeDto;
import sys.mng.restorant.mapper.Interface.RestorantTypeMapperInterface;
import sys.mng.restorant.repository.RestorantTypeRepository;
import sys.mng.restorant.service.Interface.RestorantTypeServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RestorantTypeServiceImpl implements RestorantTypeServiceInterface {
    @Autowired
    public RestorantTypeRepository repository;
    @Autowired
    public RestorantTypeMapperInterface mapper;
    @Override
    public List<RestorantTypeDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public RestorantTypeDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public RestorantTypeDto create(RestorantTypeDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RestorantTypeDto update(RestorantTypeDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RestorantTypeDto deletebyId(Integer id) {
           RestorantTypeDto delete = getbyId(id);
           repository.deleteById(id);
        return delete;
    }
}
