package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.TurniDto;

import java.util.List;

public interface TurniServiceInterface {
    List<TurniDto> getAll();
    TurniDto getbyId(Integer id);
    TurniDto create(TurniDto body);
    TurniDto update(TurniDto body);
    TurniDto deletebyId(Integer id);
}
