package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.PersoneliDto;
import sys.mng.restorant.dto.RezervimeDto;
import sys.mng.restorant.mapper.Interface.RezervimeMapperInterface;
import sys.mng.restorant.model.Personeli;
import sys.mng.restorant.model.Rezervime;
import sys.mng.restorant.repository.RezervimeRepository;
import sys.mng.restorant.service.Interface.RezervimeServiceInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RezervimeServiceImpl implements RezervimeServiceInterface {
    @Autowired
    public RezervimeRepository repository;
    @Autowired
    RezervimeMapperInterface mapper;
    @Override
    public List<RezervimeDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public RezervimeDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public RezervimeDto create(RezervimeDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RezervimeDto update(RezervimeDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RezervimeDto deletebyId(Integer id) {
        RezervimeDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
    @Override
    public Page<RezervimeDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                                     String kodi, String klientemer, String tel1, String kerkesaspecifike, Integer tavolinNo, String orari, String restorant, String vakti,
                                     String personeli) {
        return null;
    }

    @Override
    public Page<RezervimeDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                                     String kodi, String klientemer, String tel1, String kerkesaspecifike, Integer tavolinNo, String orari, String restorant, String vakti,
                                     String personeli) {


       if (RESTORANT.equals(sort))
            sort = RESTORANTDTO;

        Sort sortingOption = isAscending
                ? Sort.by(sort).ascending()
                : Sort.by(sort).descending();

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sortingOption);


        List<Rezervime> rezervimeList  = repository.searchRezervime( emer,  mbiemer,  mosha,  tel,  email,  adresa,  paga,  ditePushimi,
                muaj,turni,  pozicioni, pageable);

        List<RezervimeDto> rezervimeDtos  = new ArrayList<>();
        Integer count = repository.countRezervime( emer,  mbiemer,  mosha,  tel,  email,  adresa,  paga,  ditePushimi, muaj, turni,  pozicioni);

        for (Rezervime r : rezervimeList)
            rezervimeDtos.add(mapper.toDto(r));

        return new PageImpl<>(rezervimeDtos, pageable, count);
}
