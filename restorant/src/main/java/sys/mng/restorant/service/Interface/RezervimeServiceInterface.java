package sys.mng.restorant.service.Interface;

import org.springframework.data.domain.Page;
import sys.mng.restorant.dto.RezervimeDto;

import java.util.List;

public interface RezervimeServiceInterface {
    List<RezervimeDto> getAll();
    RezervimeDto getbyId(Integer id);
    RezervimeDto create(RezervimeDto body);
    RezervimeDto update(RezervimeDto body);
    RezervimeDto deletebyId(Integer id);


    Page<RezervimeDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                              String kodi, String klientemer, String tel1, String kerkesaspecifike, Integer tavolinNo, String orari, String restorant, String vakti,
                              String personeli);

    Page<RezervimeDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                              String kodi, String klientemer, String tel1, String kerkesaspecifike, Integer tavolinNo, String orari, String restorant, String vakti,
                              String personeli);
}
