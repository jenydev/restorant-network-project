package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.OrarOperimiDto;
import sys.mng.restorant.mapper.Interface.OrarOperimiMapperInterface;
import sys.mng.restorant.repository.OrarOperimiRepository;
import sys.mng.restorant.service.Interface.OrarOperimiServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class OrarOperimiServiceImpl implements OrarOperimiServiceInterface {

    @Autowired
    public OrarOperimiRepository repository;
    @Autowired
    public OrarOperimiMapperInterface mapper;

    @Override
    public List<OrarOperimiDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public OrarOperimiDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public OrarOperimiDto create(OrarOperimiDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public OrarOperimiDto update(OrarOperimiDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public OrarOperimiDto deletebyId(Integer id) {
           OrarOperimiDto delete = getbyId(id);
           repository.deleteById(id);
        return delete;
    }
}
