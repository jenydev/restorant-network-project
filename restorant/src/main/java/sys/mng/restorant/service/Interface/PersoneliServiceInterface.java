package sys.mng.restorant.service.Interface;

import org.springframework.data.domain.Page;
import sys.mng.restorant.dto.PersoneliDto;

import java.util.List;

public interface PersoneliServiceInterface {
    List<PersoneliDto> getAll();
    PersoneliDto getbyId(Integer id);
    PersoneliDto create(PersoneliDto body);
    PersoneliDto update(PersoneliDto body);
    PersoneliDto deletebyId(Integer id);

    List<PersoneliDto> getPersoneliByTurniId(Integer turniId);

    Page<PersoneliDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                              String emer, String mbiemer, String mosha, String tel, String email, String adresa, String paga, String ditePushimi,
                              String muaj);

    Page<PersoneliDto> filter(Integer pageSize, Integer pageNumber, String sort, Boolean isAscending,
                              String emer, String mbiemer, String mosha, String tel, String email, String adresa, String paga, String ditePushimi,
                              String muaj, String turni, String pozicioni);
}
