package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.PozicioniDto;

import java.util.List;

public interface PozicioniServiceInterface {
    List<PozicioniDto> getAll();
    PozicioniDto getbyId(Integer id);
    PozicioniDto create(PozicioniDto body);
    PozicioniDto update(PozicioniDto body);
    PozicioniDto deletebyId(Integer id);
}
