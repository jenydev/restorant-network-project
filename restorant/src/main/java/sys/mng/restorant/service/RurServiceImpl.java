package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.RURDto;
import sys.mng.restorant.mapper.Interface.RurMapperInterface;
import sys.mng.restorant.repository.RurRepository;
import sys.mng.restorant.service.Interface.RurServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RurServiceImpl implements RurServiceInterface {
    @Autowired
    public RurRepository repository;
    @Autowired
    public RurMapperInterface mapper;
    @Override
    public List<RURDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public RURDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public RURDto create(RURDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RURDto update(RURDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RURDto deletebyId(Integer id) {
        RURDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
}
