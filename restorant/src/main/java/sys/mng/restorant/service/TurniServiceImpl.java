package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.TurniDto;
import sys.mng.restorant.mapper.Interface.TurniMapperInterface;
import sys.mng.restorant.repository.TurniRepository;
import sys.mng.restorant.service.Interface.TurniServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TurniServiceImpl implements TurniServiceInterface {
    @Autowired
    public TurniRepository repository;
    @Autowired
    public TurniMapperInterface mapper;
    @Override
    public List<TurniDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public TurniDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public TurniDto create(TurniDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public TurniDto update(TurniDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public TurniDto deletebyId(Integer id) {
        TurniDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
}
