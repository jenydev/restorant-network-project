package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.RestorantDto;

import java.util.List;

//@Service
public interface RestorantServiceInterface {
    List<RestorantDto> getAll();
    RestorantDto getbyId(Integer id);
    RestorantDto create(RestorantDto body);
    RestorantDto update(RestorantDto body);
    RestorantDto deletebyId(Integer id);
    List<RestorantDto> getRestorantByRestorantType(Integer restorantTypeId);
    List<RestorantDto> getByRestorantTypeAndTavoline(Integer rtId, Integer tId);

    List<RestorantDto> getRestorantByTavolineTypeId(Integer tavolineTypeId);

    List<RestorantDto> getRestorantByMenuId(Integer mId);

    List<RestorantDto> getRestorantByTavolinId(Integer tId);

    List<RestorantDto> getRestorantByOrarOperimiId(Integer ooId);

    List<RestorantDto> getByRestorantTypeAndOrarOperimi(Integer rtId, Integer ooId);
}
