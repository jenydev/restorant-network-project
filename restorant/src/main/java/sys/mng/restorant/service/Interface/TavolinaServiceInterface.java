package sys.mng.restorant.service.Interface;

import sys.mng.restorant.dto.TavolinaDto;

import java.util.List;

public interface TavolinaServiceInterface {
    List<TavolinaDto> getAll();
    TavolinaDto getbyId(Integer id);
    TavolinaDto create(TavolinaDto body);
    TavolinaDto update(TavolinaDto body);
    TavolinaDto deletebyId(Integer id);
}
