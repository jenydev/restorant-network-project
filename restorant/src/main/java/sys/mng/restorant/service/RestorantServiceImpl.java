package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.RestorantDto;
import sys.mng.restorant.mapper.Interface.RestorantMapperInterface;
import sys.mng.restorant.repository.RestorantRepository;
import sys.mng.restorant.service.Interface.RestorantServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RestorantServiceImpl implements RestorantServiceInterface {

    @Autowired
   public final RestorantRepository repository;

    @Autowired
    public final RestorantMapperInterface mapper;


    @Override
    public List<RestorantDto> getAll() {

        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public RestorantDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public RestorantDto create(RestorantDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RestorantDto update(RestorantDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public RestorantDto deletebyId(Integer id) {
           RestorantDto delete = getbyId(id);
           repository.deleteById(id);
        return delete;
    }


    @Override
    public List<RestorantDto> getRestorantByRestorantType(Integer restorantTypeId) {
        return repository.selectRestorantByRestorantTypeId(restorantTypeId)
                .stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }


    @Override
    public List<RestorantDto> getByRestorantTypeAndTavoline(Integer rtId, Integer tId) {
        return repository.selectRestorantTypeByIdAndTavolinaId(rtId,tId)
                .stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public  List<RestorantDto> getRestorantByTavolineTypeId(Integer tavolineTypeId){
        return  repository.selectRestorantByTavolineTypeId(tavolineTypeId)
                .stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<RestorantDto> getRestorantByMenuId(Integer mId){
        return repository.selectRestorantByMenuId(mId)
                .stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<RestorantDto> getRestorantByTavolinId(Integer tId){
        return repository.selectRestorantByTavolinId(tId)
                .stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }
    @Override
    public List<RestorantDto> getRestorantByOrarOperimiId(Integer ooId){
        return repository.selectRestorantByOrarOperimiId(ooId)
                .stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<RestorantDto> getByRestorantTypeAndOrarOperimi(Integer rtId, Integer ooId) {
        return repository.selectRestorantTypeByIdAndOrarOperimiId(rtId,ooId )
                .stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }

}
