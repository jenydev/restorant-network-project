package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.TavolineTypeDto;
import sys.mng.restorant.mapper.Interface.TavolineTypeMapperInterface;
import sys.mng.restorant.repository.TavolineTypeRepository;
import sys.mng.restorant.service.Interface.TavolineTypeServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TavolinaTypeServiceImpl implements TavolineTypeServiceInterface {
    @Autowired
    public TavolineTypeRepository repository;
    @Autowired
    public TavolineTypeMapperInterface mapper;
    @Override
    public List<TavolineTypeDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public TavolineTypeDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public TavolineTypeDto create(TavolineTypeDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public TavolineTypeDto update(TavolineTypeDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public TavolineTypeDto deletebyId(Integer id) {
        TavolineTypeDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
}
