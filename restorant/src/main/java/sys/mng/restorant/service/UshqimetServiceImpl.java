package sys.mng.restorant.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sys.mng.restorant.dto.UshqimetDto;
import sys.mng.restorant.mapper.Interface.UshqimetMapperInterface;
import sys.mng.restorant.repository.UshqimetRepository;
import sys.mng.restorant.service.Interface.UshqimetServiceInterface;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UshqimetServiceImpl implements UshqimetServiceInterface {
    @Autowired
    public UshqimetRepository repository;
    @Autowired
    public UshqimetMapperInterface mapper;
    @Override
    public List<UshqimetDto> getAll() {
        return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public UshqimetDto getbyId(Integer id) {
        return mapper.toDto(repository.getReferenceById(id));
    }

    @Override
    public UshqimetDto create(UshqimetDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public UshqimetDto update(UshqimetDto body) {
        return mapper.toDto(repository.save(mapper.toEntity(body)));
    }

    @Override
    public UshqimetDto deletebyId(Integer id) {
        UshqimetDto delete = getbyId(id);
        repository.deleteById(id);
        return delete;
    }
}
