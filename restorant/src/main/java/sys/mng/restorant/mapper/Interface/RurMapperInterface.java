package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.RURDto;
import sys.mng.restorant.model.Rur;

public interface RurMapperInterface {
    Rur toEntity(RURDto dto);
    RURDto toDto(Rur entity);
}
