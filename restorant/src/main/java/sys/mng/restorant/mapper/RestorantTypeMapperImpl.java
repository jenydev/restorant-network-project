package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.RestorantTypeDto;
import sys.mng.restorant.mapper.Interface.RestorantTypeMapperInterface;
import sys.mng.restorant.model.RestorantType;

import javax.persistence.Column;

@RequiredArgsConstructor
@Component
public class RestorantTypeMapperImpl implements RestorantTypeMapperInterface {

    @Override
    public RestorantType toEntity(RestorantTypeDto dto) {
        if (dto == null)return null;

        RestorantType entity = new RestorantType();
        entity.setRestorantTypeId(dto.getId());
        entity.setRestorantType(dto.getRestorantType());

        return entity;
    }

    @Override
    public RestorantTypeDto toDto(RestorantType entity) {
        if (entity == null)return null;

        RestorantTypeDto dto = new RestorantTypeDto();
        dto.setId(entity.getRestorantTypeId());
        dto.setRestorantType(entity.getRestorantType());

        return dto;
    }
}
