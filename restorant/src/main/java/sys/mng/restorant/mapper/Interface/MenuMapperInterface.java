package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.MenuDto;
import sys.mng.restorant.model.Menu;

public interface MenuMapperInterface {
        Menu toEntity(MenuDto dto);
        MenuDto toDto(Menu entity);
}
