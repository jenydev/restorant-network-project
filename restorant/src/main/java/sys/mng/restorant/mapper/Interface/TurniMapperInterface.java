package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.TurniDto;
import sys.mng.restorant.model.Turni;

public interface TurniMapperInterface {
    Turni toEntity(TurniDto dto);
    TurniDto toDto(Turni entity);
}
