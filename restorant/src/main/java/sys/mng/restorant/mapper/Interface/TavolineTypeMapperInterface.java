package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.TavolineTypeDto;
import sys.mng.restorant.model.TavolineType;

public interface TavolineTypeMapperInterface {
    TavolineType toEntity(TavolineTypeDto dto);
    TavolineTypeDto toDto(TavolineType entity);
}
