package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.RestorantDto;
import sys.mng.restorant.model.Restorant;

public interface RestorantMapperInterface {
    Restorant toEntity(RestorantDto dto);
    RestorantDto toDto(Restorant entity);
}
