package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.TavolinaDto;
import sys.mng.restorant.mapper.Interface.TavolinaMapperInterface;
import sys.mng.restorant.model.Tavolina;
@RequiredArgsConstructor
@Component
public class TavolinaMapperImpl implements TavolinaMapperInterface {
    @Override
    public Tavolina toEntity(TavolinaDto dto) {
        if (dto == null)return null;

        Tavolina entity = new Tavolina();
        entity.setId(dto.getId());
        entity.setTavolinNo(dto.getTavolinNo());

        return entity;
    }

    @Override
    public TavolinaDto toDto(Tavolina entity) {
        if (entity == null)return null;

        TavolinaDto dto = new TavolinaDto();
        dto.setId(entity.getId());
        dto.setTavolinNo(entity.getTavolinNo());

        return dto;

    }
}
