package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.TurniDto;
import sys.mng.restorant.mapper.Interface.TurniMapperInterface;
import sys.mng.restorant.model.Turni;
@RequiredArgsConstructor
@Component
public class TurniMapperImpl implements TurniMapperInterface {
    @Override
    public Turni toEntity(TurniDto dto) {
       if (dto == null)return null;

       Turni entity = new Turni();
       entity.setId(dto.getId());
       entity.setTurni(dto.getTurni());
       entity.setOrari(dto.getOrari());

       return entity;
    }

    @Override
    public TurniDto toDto(Turni entity) {
        if (entity == null)return null;

        TurniDto dto = new TurniDto();
        dto.setId(entity.getId());
        dto.setTurni(entity.getTurni());
        dto.setOrari(entity.getOrari());

        return dto;

    }
}
