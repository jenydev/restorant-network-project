package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.OrarOperimiDto;
import sys.mng.restorant.model.OrarOperimi;

public interface OrarOperimiMapperInterface {
    OrarOperimi toEntity(OrarOperimiDto dto);
    OrarOperimiDto toDto(OrarOperimi entity);

}
