package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.OrarOperimiDto;
import sys.mng.restorant.mapper.Interface.OrarOperimiMapperInterface;
import sys.mng.restorant.model.OrarOperimi;
@RequiredArgsConstructor
@Component
public class OrarOperimiMapperImpl implements OrarOperimiMapperInterface {

    @Override
    public OrarOperimi toEntity(OrarOperimiDto dto) {
        if (dto == null)return null;

        OrarOperimi entity = new OrarOperimi();
        entity.setId(dto.getId());
        entity.setOrari(dto.getOrari());

        return entity;
    }

    @Override
    public OrarOperimiDto toDto(OrarOperimi entity) {
        if (entity == null)return null;

       OrarOperimiDto dto =new OrarOperimiDto();
       dto.setId(entity.getId());
       dto.setOrari(entity.getOrari());

       return dto;
    }
}
