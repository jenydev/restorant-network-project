package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.RestorantTypeDto;
import sys.mng.restorant.model.RestorantType;

public interface RestorantTypeMapperInterface {
    RestorantType toEntity(RestorantTypeDto dto);
    RestorantTypeDto toDto(RestorantType entity);

}
