package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.UshqimetDto;
import sys.mng.restorant.mapper.Interface.UshqimetMapperInterface;
import sys.mng.restorant.model.Ushqimet;
@RequiredArgsConstructor
@Component
public class UshqimetMapperImpl implements UshqimetMapperInterface {
    @Override
    public Ushqimet toEntity(UshqimetDto dto) {
        if (dto == null)return null;

        Ushqimet entity = new Ushqimet();
        entity.setId(dto.getId());
        entity.setUshqimet(dto.getUshqimet());

        return entity;
    }

    @Override
    public UshqimetDto toDto(Ushqimet entity) {
        if (entity == null)return null;

        UshqimetDto dto = new UshqimetDto();
        dto.setId(entity.getId());
        dto.setUshqimet(entity.getUshqimet());

        return dto;
    }
}
