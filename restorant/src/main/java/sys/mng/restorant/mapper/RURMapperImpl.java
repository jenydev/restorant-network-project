package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.RURDto;
import sys.mng.restorant.mapper.Interface.RurMapperInterface;
import sys.mng.restorant.model.Rur;
@RequiredArgsConstructor
@Component
public class RURMapperImpl implements RurMapperInterface {

    public RezervimeMapperImpl rezervimeMapper;
    public RestorantMapperImpl restorantMapper;
    public UshqimetMapperImpl ushqimetMapper;
    @Override
    public Rur toEntity(RURDto dto) {
        if (dto == null)return null;

        Rur entity = new Rur();
        entity.setId(dto.getId());
        entity.setRezervime(rezervimeMapper.toEntity(dto.getRezervimeDto()));
        entity.setUshqimet(ushqimetMapper.toEntity(dto.getUshqimetDto()));
        entity.setRestorant(restorantMapper.toEntity(dto.getRestorantDto()));
        entity.setCodeId(dto.getCodeId());
        entity.setSasia(dto.getSasia());
        return entity;
    }

    @Override
    public RURDto toDto(Rur entity) {
        if (entity == null)return null;

        RURDto dto = new RURDto();
        dto.setId(entity.getId());
        dto.setRezervimeDto(rezervimeMapper.toDto(entity.getRezervime()));
        dto.setUshqimetDto(ushqimetMapper.toDto(entity.getUshqimet()));
        dto.setRestorantDto(restorantMapper.toDto(entity.getRestorant()));
        dto.setCodeId(entity.getCodeId());
        dto.setSasia(entity.getSasia());

        return dto;
    }
}
