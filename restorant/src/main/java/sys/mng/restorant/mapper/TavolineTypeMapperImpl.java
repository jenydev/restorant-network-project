package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.TavolineTypeDto;
import sys.mng.restorant.mapper.Interface.TavolineTypeMapperInterface;
import sys.mng.restorant.model.TavolineType;
@RequiredArgsConstructor
@Component
public class TavolineTypeMapperImpl implements TavolineTypeMapperInterface {
    @Override
    public TavolineType toEntity(TavolineTypeDto dto) {
        if (dto == null)return null;

        TavolineType entity = new TavolineType();
        entity.setId(dto.getId());
        entity.setTavolineType(dto.getTavolineType());

        return entity;
    }

    @Override
    public TavolineTypeDto toDto(TavolineType entity) {
        if (entity == null)return null;

        TavolineTypeDto dto = new TavolineTypeDto();
        dto.setId(entity.getId());
        dto.setTavolineType(entity.getTavolineType());

        return dto;
    }
}
