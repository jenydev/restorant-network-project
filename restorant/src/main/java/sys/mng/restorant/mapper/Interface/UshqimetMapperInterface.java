package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.UshqimetDto;
import sys.mng.restorant.model.Ushqimet;

public interface UshqimetMapperInterface {
    Ushqimet toEntity(UshqimetDto dto);
    UshqimetDto toDto(Ushqimet entity);
}
