package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.PersoneliDto;
import sys.mng.restorant.model.Personeli;

public interface PersoneliMapperInterface {
    Personeli toEntity(PersoneliDto dto);
    PersoneliDto toDto(Personeli entity);

}
