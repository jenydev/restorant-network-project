package sys.mng.restorant.mapper;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.RestorantDto;
import sys.mng.restorant.mapper.Interface.*;
import sys.mng.restorant.model.Restorant;

@RequiredArgsConstructor
//@NoArgsConstructor
@Component
public class RestorantMapperImpl implements RestorantMapperInterface {

    public final TavolinaMapperInterface tavolinaMapperInterface;
    public final TavolineTypeMapperInterface tavolineTypeMapperInterface;
    public final MenuMapperInterface menuMapperInterface;
    public final OrarOperimiMapperInterface orarOperimiMapperInterface;
    public final RestorantTypeMapperInterface restorantTypeMapperInterface;

    @Override
    public Restorant toEntity(RestorantDto dto) {
        if (dto == null)return null;

        Restorant entity = new Restorant();
        entity.setRestorantId(dto.getId());
        entity.setEmer(dto.getEmer());
        entity.setCodeFilial(dto.getCodeFilial());
        entity.setNipt(dto.getNipt());
        entity.setAddress(dto.getAddress());
        entity.setQyteti(dto.getQyteti());
        entity.setRruga(dto.getRruga());
        entity.setTelOne(dto.getTelOne());
        entity.setTelTwo(dto.getTelTwo());
        entity.setTavolina(tavolinaMapperInterface.toEntity(dto.getTavolinaDto()));
        entity.setTavolineType(tavolineTypeMapperInterface.toEntity(dto.getTavolineTypeDto()));
        entity.setMenu(menuMapperInterface.toEntity(dto.getMenuDto()));
        entity.setOrarOperimi(orarOperimiMapperInterface.toEntity(dto.getOrarOperimiDto()));
        entity.setRestorantType(restorantTypeMapperInterface.toEntity(dto.getRestorantTypeDto()) );

        return entity;
    }

    @Override
    public RestorantDto toDto(Restorant entity) {
        if (entity == null)return null;

        RestorantDto dto = new RestorantDto();
        dto.setId(entity.getRestorantId());
        dto.setEmer(entity.getEmer());
        dto.setCodeFilial(entity.getCodeFilial());
        dto.setNipt(entity.getNipt());
        dto.setAddress(entity.getAddress());
        dto.setQyteti(entity.getQyteti());
        dto.setRruga(entity.getRruga());
        dto.setTelOne(entity.getTelOne());
        dto.setTelTwo(entity.getTelTwo());
        dto.setTavolinaDto(tavolinaMapperInterface.toDto(entity.getTavolina()));
        dto.setTavolineTypeDto(tavolineTypeMapperInterface.toDto(entity.getTavolineType()));
        dto.setMenuDto(menuMapperInterface.toDto(entity.getMenu()));
        dto.setOrarOperimiDto(orarOperimiMapperInterface.toDto(entity.getOrarOperimi()));
        dto.setRestorantTypeDto(restorantTypeMapperInterface.toDto(entity.getRestorantType()));

        return dto;
    }
}
