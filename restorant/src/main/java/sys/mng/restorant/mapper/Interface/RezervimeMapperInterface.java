package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.RezervimeDto;
import sys.mng.restorant.model.Rezervime;

public interface RezervimeMapperInterface {
    Rezervime toEntity(RezervimeDto dto);
    RezervimeDto toDto(Rezervime entity);
}
