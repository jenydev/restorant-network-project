package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.TavolinaDto;
import sys.mng.restorant.model.Tavolina;

public interface TavolinaMapperInterface {
    Tavolina toEntity(TavolinaDto dto);
    TavolinaDto toDto(Tavolina entity);

}
