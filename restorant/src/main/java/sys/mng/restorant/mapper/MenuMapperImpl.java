package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.MenuDto;
import sys.mng.restorant.mapper.Interface.MenuMapperInterface;
import sys.mng.restorant.model.Menu;
@RequiredArgsConstructor
@Component
public class MenuMapperImpl implements MenuMapperInterface {
    @Override
    public Menu toEntity(MenuDto dto) {
        if (dto == null)return null;

        Menu entity = new Menu();
        entity.setId(dto.getId());
        entity.setMenuType(dto.getMenuType());

        return entity;
    }

    @Override
    public MenuDto toDto(Menu entity) {
        if (entity == null)return null;

        MenuDto dto = new MenuDto();
        dto.setId(entity.getId());
        dto.setMenuType(entity.getMenuType());

        return dto;
    }
}
