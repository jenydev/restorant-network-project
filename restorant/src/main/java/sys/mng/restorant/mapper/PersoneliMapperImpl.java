package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.PersoneliDto;
import sys.mng.restorant.mapper.Interface.PersoneliMapperInterface;
import sys.mng.restorant.model.Personeli;
@RequiredArgsConstructor
@Component
public class PersoneliMapperImpl implements PersoneliMapperInterface {
    @Autowired
    public final TurniMapperImpl turniMapper;
    @Autowired
    public final PozicioniMapperImpl pozicioniMapper;

    @Override
    public Personeli toEntity(PersoneliDto dto) {
     if (dto == null)return null;

     Personeli entity = new Personeli();
        entity.setId(dto.getId());
        entity.setEmer(dto.getEmer());
        entity.setMbiemer(dto.getMbiemer());
        entity.setMosha(dto.getMosha());
        entity.setGjinia(dto.getGjinia());
        entity.setTel(dto.getTel());
        entity.setEmail(dto.getEmail());
        entity.setAdresa(dto.getAdresa());
        entity.setPaga(dto.getPaga());
        entity.setDitePushimi(dto.getDitePushimi());
        entity.setMuaj(dto.getMuaj());
        entity.setTurni(turniMapper.toEntity(dto.turniDto));
        entity.setPozicioni(pozicioniMapper.toEntity(dto.pozicioniDto));

        return entity;
    }

    @Override
    public PersoneliDto toDto(Personeli entity) {
        if (entity == null)return null;

        PersoneliDto dto = new PersoneliDto();
        dto.setId(entity.getId());
        dto.setEmer(entity.getEmer());
        dto.setMbiemer(entity.getMbiemer());
        dto.setMosha(entity.getMosha());
        dto.setGjinia(entity.getGjinia());
        dto.setTel(entity.getTel());
        dto.setEmail(entity.getEmail());
        dto.setAdresa(entity.getAdresa());
        dto.setPaga(entity.getPaga());
        dto.setDitePushimi(entity.getDitePushimi());
        dto.setMuaj(entity.getMuaj());

        dto.setTurniDto(turniMapper.toDto(entity.getTurni()));
        dto.setPozicioniDto(pozicioniMapper.toDto(entity.getPozicioni()));

        return dto;
    }
}
