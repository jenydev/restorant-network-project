package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.VaktiDto;
import sys.mng.restorant.mapper.Interface.VaktiMapperInterface;
import sys.mng.restorant.model.Vakti;

@RequiredArgsConstructor
@Component
public class VaktiMapperImpl implements VaktiMapperInterface {

    @Override
    public Vakti toEntity(VaktiDto dto) {
      if (dto == null)return null;

      Vakti entity = new Vakti();
      entity.setId(dto.getId());
      entity.setVakti(dto.getVakti());

      return entity;
    }

    @Override
    public VaktiDto toDto(Vakti entity) {
     if (entity == null)return null;

     VaktiDto dto = new VaktiDto();
     dto.setId(entity.getId());
     dto.setVakti(entity.getVakti());

     return dto;
    }
}
