package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.VaktiDto;
import sys.mng.restorant.model.Vakti;

public interface VaktiMapperInterface {
    Vakti toEntity(VaktiDto dto);
    VaktiDto toDto(Vakti entity);
}
