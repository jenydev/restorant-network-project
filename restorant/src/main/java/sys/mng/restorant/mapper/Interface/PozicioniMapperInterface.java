package sys.mng.restorant.mapper.Interface;

import sys.mng.restorant.dto.PozicioniDto;
import sys.mng.restorant.model.Pozicioni;

public interface PozicioniMapperInterface {
    Pozicioni toEntity(PozicioniDto dto);
    PozicioniDto toDto(Pozicioni entity);

}
