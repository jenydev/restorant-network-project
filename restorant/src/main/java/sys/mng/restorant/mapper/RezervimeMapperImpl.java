package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.RezervimeDto;
import sys.mng.restorant.mapper.Interface.RezervimeMapperInterface;
import sys.mng.restorant.model.Rezervime;
@RequiredArgsConstructor
@Component
public class RezervimeMapperImpl implements RezervimeMapperInterface {

    public final RestorantMapperImpl restorantMapper;
    public final TavolinaMapperImpl tavolinaMapper;

    @Override
    public Rezervime toEntity(RezervimeDto dto) {
        if (dto == null)return null;

        Rezervime entity = new Rezervime();
        entity.setId(dto.getId());
        entity.setKodi(dto.getKodi());
        entity.setKlientEmer(dto.getKlientEmer());
        entity.setTel1(dto.getTel1());
        entity.setKerkesaSpecifike(dto.getKerkesaSpecifike());
        entity.setTavolinNo(tavolinaMapper.toEntity(dto.getTavolinaDto()));
        entity.setOrari(dto.getOrari());
        entity.setRestorant(restorantMapper.toEntity(dto.getRestorantDto()));

        return entity;
    }

    @Override
    public RezervimeDto toDto(Rezervime entity) {
        if (entity == null)return null;

        RezervimeDto dto = new RezervimeDto();
        dto.setId(entity.getId());
        dto.setKodi(entity.getKodi());
        dto.setKlientEmer(entity.getKlientEmer());
        dto.setTel1(entity.getTel1());
        dto.setKerkesaSpecifike(entity.getKerkesaSpecifike());
        dto.setTavolinaDto(tavolinaMapper.toDto(entity.getTavolinNo()));
        dto.setOrari(entity.getOrari());
        dto.setRestorantDto(restorantMapper.toDto(entity.getRestorant()));
        return dto;
    }
}
