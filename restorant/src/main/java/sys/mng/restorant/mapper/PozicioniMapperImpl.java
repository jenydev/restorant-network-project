package sys.mng.restorant.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sys.mng.restorant.dto.PozicioniDto;
import sys.mng.restorant.mapper.Interface.PozicioniMapperInterface;
import sys.mng.restorant.model.Pozicioni;
@RequiredArgsConstructor
@Component
public class PozicioniMapperImpl implements PozicioniMapperInterface {
    @Override
    public Pozicioni toEntity(PozicioniDto dto) {
        if (dto == null)return null;

        Pozicioni entity = new Pozicioni();
        entity.setId(dto.getId());
        entity.setProfesioni(dto.getProfesioni());

        return entity;
    }

    @Override
    public PozicioniDto toDto(Pozicioni entity) {
        if (entity == null)return null;

        PozicioniDto dto = new PozicioniDto();
        dto.setId(entity.getId());
        dto.setProfesioni(entity.getProfesioni());

        return dto;
    }
}
