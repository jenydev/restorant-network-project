package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.TavolineTypeDto;
import sys.mng.restorant.service.Interface.TavolineTypeServiceInterface;

import java.util.List;
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class TavolinaTypeController {
    @Autowired
    public static TavolineTypeServiceInterface service;

    @GetMapping("/getAllTavolinaType")
    public List<TavolineTypeDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getTavolinaTypebyId")
    public TavolineTypeDto getTavolinaTypebyId(@RequestParam(name = "TavolinaTypeId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getTavolinaTypeById/{id}")
    public TavolineTypeDto getTavolinaTypeById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createTavolinaType")
    public TavolineTypeDto saveTavolinaType(@Validated @RequestBody TavolineTypeDto body){
        return service.create(body);
    }
    @PutMapping("/updateTavolinaType")
    public  TavolineTypeDto updateTavolinaType(@Validated @RequestBody TavolineTypeDto body){
        return service.update(body);
    }
    @PatchMapping("/updateTavolinaType")
    public  TavolineTypeDto updateTavolinaType1(@Validated @RequestBody TavolineTypeDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteTavolinaTypeById/{id}")
    public  TavolineTypeDto deleteTavolinaTypeById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteTavolinaTypebyId")
    public TavolineTypeDto deleteTavolinaTypebyId(@RequestParam(name = "TavolinaTypeId") Integer id) {
        return service.deletebyId(id);
    }
}
