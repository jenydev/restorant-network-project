package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.RestorantDto;
import sys.mng.restorant.service.Interface.RestorantServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class RestorantController {

    @Autowired
    public RestorantServiceInterface service;

    @GetMapping("/getAllRestorant")
    public List<RestorantDto> getAll() {
        return service.getAll();
    }
    @GetMapping("/getRestorantbyId")
    public RestorantDto getRestorantbyId(@RequestParam(name = "restorantId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getRestorantById/{id}")
    public RestorantDto getRestorantById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createRestorant")
    public RestorantDto saveRestorant(@Validated @RequestBody RestorantDto body){
        return service.create(body);
    }
    @PutMapping("/updateRestorant")
    public  RestorantDto updateRestorant(@Validated @RequestBody RestorantDto body){
        return service.update(body);
    }
    @PatchMapping("/updateRestorant")
    public  RestorantDto updateRestorant1(@Validated @RequestBody RestorantDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteRestorantById/{id}")
    public RestorantDto deleteRestorantById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteRestorantbyId")
    public RestorantDto deleteRestorantbyId(@RequestParam(name = "restorantId") Integer id) {
        return service.deletebyId(id);
    }

    @GetMapping("/getByRestorantTypeId")
    public List<RestorantDto> getByRestorantTypeId(@RequestParam Integer restorantTypeId) {
        return service.getRestorantByRestorantType(restorantTypeId);
    }
    @GetMapping("/getByTavolineTypeId")
    public  List<RestorantDto> getByTavolineTypeId(@RequestParam Integer tavolineTypeId){
        return service.getRestorantByTavolineTypeId(tavolineTypeId);
    }

    @GetMapping("getByMenuId")
    public List<RestorantDto> getByMenuId(@RequestParam Integer menuId){
        return service.getRestorantByMenuId(menuId);
    }

    @GetMapping("getByTavolinId")
    public  List<RestorantDto> getByTavolinId(@RequestParam Integer tId){
        return service.getRestorantByTavolinId(tId);
    }

    @GetMapping("getByOrarOperimiId")
    public List<RestorantDto> getByOrarOperimiId(@RequestParam Integer ooId) {
        return service.getRestorantByOrarOperimiId(ooId);
    }

    @GetMapping("getRestorantTypeByOrarOperimiId")
    public List<RestorantDto> getRestorantTypeByOrarOperimiId(@RequestParam Integer rId, Integer oId) {
        return service.getByRestorantTypeAndOrarOperimi(rId,oId);
    }
    @GetMapping("getRestorantTypeByIdAndTavolinaId")
    public List<RestorantDto> getRestorantTypeByIdAndTavolinaId(@RequestParam Integer rtId, Integer rId) {
        return service.getByRestorantTypeAndTavoline(rtId,rId);
    }


}
