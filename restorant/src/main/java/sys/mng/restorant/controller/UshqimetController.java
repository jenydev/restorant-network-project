package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.UshqimetDto;
import sys.mng.restorant.service.Interface.UshqimetServiceInterface;

import java.util.List;
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class UshqimetController {
    @Autowired
    public static UshqimetServiceInterface service;

    @GetMapping("/getAllUshqimet")
    public List<UshqimetDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getUShqimetbyId")
    public UshqimetDto getUshqimetbyId(@RequestParam(name = "ushqimetId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getUshqimetById/{id}")
    public UshqimetDto getUshqimetById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createUshqimet")
    public UshqimetDto saveUshqimet(@Validated @RequestBody UshqimetDto body){
        return service.create(body);
    }
    @PutMapping("/updateUshqimet")
    public  UshqimetDto updateUshqimet(@Validated @RequestBody UshqimetDto body){
        return service.update(body);
    }
    @PatchMapping("/updateUshqimet")
    public  UshqimetDto updateUshqimet1(@Validated @RequestBody UshqimetDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteUshqimetById/{id}")
    public UshqimetDto deleteUshqimetById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteUshqimetbyId")
    public UshqimetDto deleteUshqimetbyId(@RequestParam(name = "ushqimetId") Integer id) {
        return service.deletebyId(id);
    }
}
