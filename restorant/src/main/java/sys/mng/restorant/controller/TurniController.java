package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.TurniDto;
import sys.mng.restorant.service.Interface.TurniServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class TurniController {

    @Autowired
    public static TurniServiceInterface service;

    @GetMapping("/getAllTurni")
    public List<TurniDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getTurnibyId")
    public TurniDto getTurnibyId(@RequestParam(name = "turniId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getTurniById/{id}")
    public TurniDto getTurniById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createTurni")
    public TurniDto saveTurni(@Validated @RequestBody TurniDto body){
        return service.create(body);
    }
    @PutMapping("/updateTurni")
    public  TurniDto updateTurni(@Validated @RequestBody TurniDto body){
        return service.update(body);
    }
    @PatchMapping("/updateTurni")
    public  TurniDto updateTurni1(@Validated @RequestBody TurniDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteTurniById/{id}")
    public TurniDto deleteTurniById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteTurnibyId")
    public TurniDto deleteTurnibyId(@RequestParam(name = "turniId") Integer id) {
        return service.deletebyId(id);
    }
}
