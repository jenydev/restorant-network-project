package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.PersoneliDto;
import sys.mng.restorant.dto.RezervimeDto;
import sys.mng.restorant.service.Interface.RezervimeServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class RezervimeController {
    @Autowired
    public static RezervimeServiceInterface service;

    @GetMapping("/getAllRezervime")
    public List<RezervimeDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getRezervimebyId")
    public RezervimeDto getRezervimebyId(@RequestParam(name = "rezervimeId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getRezervimeById/{id}")
    public RezervimeDto getRezervimeById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createRezervime")
    public RezervimeDto saveRezervime(@Validated @RequestBody RezervimeDto body){
        return service.create(body);
    }
    @PutMapping("/updateRezervime")
    public  RezervimeDto updateRezervime(@Validated @RequestBody RezervimeDto body){
        return service.update(body);
    }
    @PatchMapping("/updateRezervime")
    public  RezervimeDto updateRezervime1(@Validated @RequestBody RezervimeDto body){ return service.update(body); }
    @DeleteMapping("/deleteRezervimeById/{id}")
    public RezervimeDto deleteRezervimeById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteRezervimebyId")
    public RezervimeDto deleteRezervimebyId(@RequestParam(name = "rezervimeId") Integer id) {
        return service.deletebyId(id);
    }
    @GetMapping("/filter")
    public Page<RezervimeDto> filter(@RequestParam Integer pageSize, @RequestParam Integer pageNumber,
                                     @RequestParam String sort, Boolean isAscending,
                                     @RequestParam(name = "kodi", required = false) String kodi,
                                     @RequestParam(name = "klientemer", required = false) String klientemer,
                                     @RequestParam(name = "tel1", required = false) String tel1,
                                     @RequestParam(name = "kerkesaspecifike", required = false) String kerkesaspecifike,
                                     @RequestParam(name = "tavolinNo", required = false) Integer tavolinNo,
                                     @RequestParam(name = "orari", required = false) String orari,
                                     @RequestParam(name = "restorant", required = false) String restorant,
                                     @RequestParam(name = "vakti", required = false) String vakti,
                                     @RequestParam(name = "personeli", required = false) String personeli) {
        return service.filter(pageSize, pageNumber, sort, isAscending,kodi, klientemer,tel1,
                kerkesaspecifike,tavolinNo, orari, restorant,vakti, personeli );

    }
}
