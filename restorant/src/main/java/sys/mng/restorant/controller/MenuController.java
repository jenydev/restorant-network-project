package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.MenuDto;
import sys.mng.restorant.service.Interface.MenuServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    public  MenuServiceInterface service;

    @GetMapping("/getAllMenu")
    public List<MenuDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getMenubyId")
    public MenuDto getMenubyId(@RequestParam(name = "menuId") Integer id) {
        return service.getById(id);
    }
    @GetMapping("/getMenuById/{id}")
    public MenuDto getMenuById(@PathVariable("id") Integer id){
        return service.getById(id);
    }
    @PostMapping("/createMenu")
    public MenuDto saveMenu(@Validated @RequestBody MenuDto body){
        return service.create(body);
    }
    @PutMapping("/updateMenu")
    public  MenuDto updateMenu(@Validated @RequestBody MenuDto body){
        return service.update(body);
    }
    @PatchMapping("/updateMenu")
    public  MenuDto updateMenu1(@Validated @RequestBody MenuDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteMenuById/{id}")
    public MenuDto deleteMenuById(@PathVariable("id") Integer id) {
        return service.deleteById(id);
    }
    @DeleteMapping("/deleteMenubyId")
    public MenuDto deleteMenubyId(@RequestParam(name = "menuId") Integer id) {
        return service.deleteById(id);
    }
}
