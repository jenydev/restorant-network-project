package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.VaktiDto;
import sys.mng.restorant.service.Interface.VaktiServiceInterface;
import sys.mng.restorant.service.VaktiServiceImpl;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class VaktiController {

    @Autowired
    public VaktiServiceImpl service;

    @GetMapping("/getAllVakti")
    public List<VaktiDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getVaktibyId")
    public VaktiDto getVaktibyId(@RequestParam(name = "vaktiId") Integer id) {
        return service.getbyId(id);
    }

    @GetMapping("/getVaktoById/{id}")
    public VaktiDto getVaktiById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }

    @PostMapping("/createVakti")
    public VaktiDto saveVakti(@Validated @RequestBody VaktiDto body){
        return service.create(body);
    }

    @PutMapping("/updateVakti")
    public  VaktiDto updateVakti(@Validated @RequestBody VaktiDto body){
        return service.update(body);
    }

    @PatchMapping("/updateVakti")
    public  VaktiDto updateVakti1(@Validated @RequestBody VaktiDto body){
        return service.update(body);
    }

    @DeleteMapping("/deleteVaktiById/{id}")
    public VaktiDto deleteVaktiById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }

    @DeleteMapping("/deleteVaktibyId")
    public VaktiDto deleteVaktibyId(@RequestParam(name = "vaktiId") Integer id) {
        return service.deletebyId(id);
    }
}
