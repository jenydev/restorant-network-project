package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.RestorantTypeDto;
import sys.mng.restorant.service.Interface.RestorantTypeServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class RestorantTypeController {
    @Autowired
    public static RestorantTypeServiceInterface service;

    @GetMapping("/getAllRestorantType")
    public List<RestorantTypeDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getRestorantTypebyId")
    public RestorantTypeDto getRestorantTypebyId(@RequestParam(name = "RestorantTypeId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getRestorantTypeById/{id}")
    public RestorantTypeDto getRestorantTypeById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createRestorantType")
    public RestorantTypeDto saveRestorantType(@Validated @RequestBody RestorantTypeDto body){
        return service.create(body);
    }
    @PutMapping("/updateRestorantType")
    public  RestorantTypeDto updateRestorantType(@Validated @RequestBody RestorantTypeDto body){
        return service.update(body);
    }
    @PatchMapping("/updateRestorantType")
    public RestorantTypeDto updateRestorantType1(@Validated @RequestBody RestorantTypeDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteRestorantTypeById/{id}")
    public RestorantTypeDto deleteRestorantTypeById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteRestorantTypebyId")
    public RestorantTypeDto deleteRestorantTypebyId(@RequestParam(name = "RestorantTypeId") Integer id) {
        return service.deletebyId(id);
    }
}
