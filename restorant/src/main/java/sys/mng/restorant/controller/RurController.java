package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.RURDto;
import sys.mng.restorant.service.Interface.RurServiceInterface;

import java.util.List;
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class RurController {
    @Autowired
    public static RurServiceInterface service;

    @GetMapping("/getAllRur")
    public List<RURDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getRurbyId")
    public RURDto getRurbyId(@RequestParam(name = "rurId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getRurById/{id}")
    public RURDto getRurById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createRur")
    public RURDto saveRur(@Validated @RequestBody RURDto body){
        return service.create(body);
    }
    @PutMapping("/updateRur")
    public  RURDto updateRur(@Validated @RequestBody RURDto body){
        return service.update(body);
    }
    @PatchMapping("/updateRur")
    public  RURDto updateRur1(@Validated @RequestBody RURDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteRurById/{id}")
    public RURDto deleteRurById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteRurbyId")
    public RURDto deleteRurbyId(@RequestParam(name = "rurId") Integer id) {
        return service.deletebyId(id);
    }
}
