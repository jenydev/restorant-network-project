package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.TavolinaDto;
import sys.mng.restorant.service.Interface.TavolinaServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class TavolinaController {
    @Autowired
    public static TavolinaServiceInterface service;

    @GetMapping("/getAllTavolina")
    public List<TavolinaDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getTavolinabyId")
    public TavolinaDto getTavolinabyId(@RequestParam(name = "tavolinaId") Integer id) {
        return service.getbyId(id);
    }
    @GetMapping("/getTavolinaById/{id}")
    public TavolinaDto getTavolinaById(@PathVariable("id") Integer id){
        return service.getbyId(id);
    }
    @PostMapping("/createTavolina")
    public TavolinaDto saveTavolina(@Validated @RequestBody TavolinaDto body){
        return service.create(body);
    }
    @PutMapping("/updateTavolina")
    public  TavolinaDto updateTavolina(@Validated @RequestBody TavolinaDto body){
        return service.update(body);
    }
    @PatchMapping("/updateTavolina")
    public  TavolinaDto updateTavolina1(@Validated @RequestBody TavolinaDto body){
        return service.update(body);
    }
    @DeleteMapping("/deleteTavolinaById/{id}")
    public TavolinaDto deleteTavolinaById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }
    @DeleteMapping("/deleteTavolinabyId")
    public TavolinaDto deleteTavolinabyId(@RequestParam(name = "tavolinaId") Integer id) {
        return service.deletebyId(id);
    }
}
