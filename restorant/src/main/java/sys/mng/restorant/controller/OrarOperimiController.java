package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.OrarOperimiDto;
import sys.mng.restorant.service.Interface.OrarOperimiServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class OrarOperimiController {

    @Autowired
    public static OrarOperimiServiceInterface service;

    @GetMapping("/getAllOrarOperimi")
    public List<OrarOperimiDto> getAll(){
        return service.getAll();
    }
    @GetMapping("/getOrarOperimibyId")
    public OrarOperimiDto getOrarOperimibyId(@RequestParam(name = "OrarOperimiId") Integer id){
        return service.getbyId(id);
    }
    @GetMapping("/getOrarOperimi/{id}")
    public OrarOperimiDto getOrarOperimiById(@PathVariable("id")Integer id){ return service.getbyId(id);}
    @PostMapping("/createOrarOperimi")
    public OrarOperimiDto saveOrarOperimi(@Validated @RequestBody OrarOperimiDto body){ return service.create(body);}
    @PutMapping("/updateOrarOperimi")
    public  OrarOperimiDto updateOrarOperimi(@Validated @RequestBody OrarOperimiDto body){ return service.update(body);}
    @PatchMapping("/updateOrarOperimi")
    public OrarOperimiDto updateOrarOperimi1(@Validated @RequestBody OrarOperimiDto body){ return service.update(body);}
    @DeleteMapping("/deleteOrarOperimibyId/{id}")
    public OrarOperimiDto deleteOrarOperimiById(@PathVariable("id") Integer id){ return service.deletebyId(id);}
    @DeleteMapping("/deleteOrarOperimibyId")
    public OrarOperimiDto deleteOrarOperimibyId(@RequestParam(name = "OrarOperimiId") Integer id){ return service.deletebyId(id);}
}
