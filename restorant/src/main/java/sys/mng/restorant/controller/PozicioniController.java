package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.PozicioniDto;
import sys.mng.restorant.service.Interface.PozicioniServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class PozicioniController {
    @Autowired
    public static PozicioniServiceInterface service;

    @GetMapping("/getAllPozicioni")
    public List<PozicioniDto> getAll(){
        return service.getAll();
    }
    @GetMapping("/getPozicionibyId")
    public PozicioniDto getPozicionibyId(@RequestParam(name = "pozicioniId") Integer id){
        return service.getbyId(id);
    }
    @GetMapping("/getPozicioniById/{id}")
    public PozicioniDto getPozicioniById(@PathVariable("id") Integer id){ return service.getbyId(id);}
    @PostMapping("/createPozicioni")
    public PozicioniDto savePozicioni(@Validated @RequestBody PozicioniDto body){ return service.create(body);}
    @PutMapping("/updatePozicioni")
    public PozicioniDto updatePozicioni(@Validated @RequestBody PozicioniDto body){ return  service.update(body);}
    @PatchMapping("updatePozicioni")
    public PozicioniDto updatePozicioni1(@Validated @RequestBody PozicioniDto body){ return  service.update(body);}
    @DeleteMapping("/deletePozicioniById/{id}")
    public PozicioniDto deletePozicioniById(@PathVariable("id") Integer id) { return service.deletebyId(id);}
    @DeleteMapping("/deletePozicionibyId")
    public  PozicioniDto deletePozicionibyId(@RequestParam(name = "pozicioniId") Integer id) {
        return service.deletebyId(id);
    }
}
