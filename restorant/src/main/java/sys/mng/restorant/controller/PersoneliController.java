package sys.mng.restorant.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sys.mng.restorant.dto.PersoneliDto;
import sys.mng.restorant.service.Interface.PersoneliServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/restorant")
public class PersoneliController {
    @Autowired
    public PersoneliServiceInterface service;

    @GetMapping("/getAllPersoneli")
    public List<PersoneliDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/getPersonelibyId")
    public PersoneliDto getPersonelibyId(@RequestParam(name = "personeliId") Integer id) {
        return service.getbyId(id);
    }

    @GetMapping("/getPersoneliById/{id}")
    public PersoneliDto getPersoneliById(@PathVariable("id") Integer id) {
        return service.getbyId(id);
    }

    @PostMapping("/createPersoneli")
    public PersoneliDto savePersoneli(@Validated @RequestBody PersoneliDto body) {
        return service.create(body);
    }

    @PutMapping("/updatePersoneli")
    public PersoneliDto updatePersoneli(@Validated @RequestBody PersoneliDto body) {
        return service.update(body);
    }

    @PatchMapping("/updatePersoneli")
    public PersoneliDto updatePersoneli1(@Validated @RequestBody PersoneliDto body) {
        return service.update(body);
    }

    @DeleteMapping("/deletePersoneliById/{id}")
    public PersoneliDto deletePersoneliById(@PathVariable("id") Integer id) {
        return service.deletebyId(id);
    }

    @DeleteMapping("/deletePersonelibyId")
    public PersoneliDto deletePersonelibyId(@RequestParam(name = "personeliId") Integer id) {
        return service.deletebyId(id);
    }

    @GetMapping("/getByTurniId")
    public List<PersoneliDto> getByTurniId(@RequestParam Integer turniId){
        return service.getPersoneliByTurniId(turniId);
    }

    @GetMapping("/filter")
    public Page<PersoneliDto> filter(@RequestParam Integer pageSize, @RequestParam Integer pageNumber,
                                     @RequestParam String sort, Boolean isAscending,
                                     @RequestParam(name = "emer", required = false) String emer,
                                     @RequestParam(name = "mbiemer", required = false) String mbiemer,
                                     @RequestParam(name = "mosha", required = false) String mosha,
                                     @RequestParam(name = "tel", required = false) String tel,
                                     @RequestParam(name = "email", required = false) String email,
                                     @RequestParam(name = "adresa", required = false) String adresa,
                                     @RequestParam(name = "paga", required = false) String paga,
                                     @RequestParam(name = "ditePushimi", required = false) String ditePushimi,
                                     @RequestParam(name = "muaj", required = false) String muaj)
                                     @RequestParam(name = "turni", required = false) String turni,
                                     @RequestParam(name = "pozicioni", required = false) String pozicioni) {
        return service.filter(pageSize, pageNumber, sort, isAscending, emer, mbiemer, mosha, tel, email, adresa, paga, ditePushimi,
                muaj,turni,pozicioni);

    }



