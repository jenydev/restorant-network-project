package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.OrarOperimi;

public interface OrarOperimiRepository extends JpaRepository<OrarOperimi,Integer> {
}
