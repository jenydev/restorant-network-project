package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sys.mng.restorant.model.Restorant;

import java.util.List;

@Repository
public interface RestorantRepository extends JpaRepository<Restorant, Integer> {

    @Query("SELECT p FROM Restorant p WHERE p.restorantType.restorantTypeId = ?1")
    List<Restorant> selectRestorantByRestorantTypeId(@Param("restorantTypeId") Integer restorantTypeId);

    @Query("SELECT p FROM Restorant p WHERE p.restorantType.restorantTypeId = :Id AND p.tavolina.id = :Id")
    List<Restorant> selectRestorantTypeByIdAndTavolinaId(@Param("Id") Integer rtId, @Param("Id") Integer tId);

    @Query("SELECT p FROM Restorant p WHERE p.tavolina.id= ?1" )
    List<Restorant> selectRestorantByTavolineTypeId(@Param("id") Integer id);

    @Query("SELECT o FROM Restorant o WHERE o.orarOperimi.id = :Id AND o.restorantType.restorantTypeId = :Id")
    List<Restorant> selectRestorantTypeByIdAndOrarOperimiId(@Param("Id") Integer rId, @Param("Id") Integer oId);

    @Query("SELECT p FROM Restorant p WHERE p.menu.id = :Id")
    List<Restorant> selectRestorantByMenuId(@Param("Id") Integer mid);

    @Query("SELECT t FROM Restorant t WHERE t.tavolina.id = :Id")
    List<Restorant> selectRestorantByTavolinId(@Param("Id") Integer tId);

    @Query("SELECT o FROM Restorant o WHERE o.orarOperimi.id = :Id")
    List<Restorant> selectRestorantByOrarOperimiId(@Param("Id") Integer ooId);



}
