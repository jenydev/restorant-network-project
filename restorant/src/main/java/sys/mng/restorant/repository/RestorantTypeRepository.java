package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.RestorantType;

public interface RestorantTypeRepository extends JpaRepository<RestorantType, Integer> {
}
