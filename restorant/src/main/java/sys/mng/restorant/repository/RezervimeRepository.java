package sys.mng.restorant.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import sys.mng.restorant.mapper.Interface.PersoneliMapperInterface;
import sys.mng.restorant.mapper.Interface.RezervimeMapperInterface;
import sys.mng.restorant.model.Personeli;
import sys.mng.restorant.model.Rezervime;

import java.util.List;

public interface RezervimeRepository extends JpaRepository<Rezervime,Integer> {

    @Autowired
    public RezervimeRepository repository;

    @Autowired
    public RezervimeMapperInterface mapper;


    //sort variable
    private static final String RESTORANT = "restorant";
    private static final String RESTORANTDTO = "restorantDto." + RESTORANT;

    private static final String KODI = "kodi";
    private static final String KLIENTEMER = "klientemer";
    private static final String TEL1 = "tel1";
    private static final String KERKESASPECIFIKE = "kerkesaspecifike";
    private static final Integer TAVOLINNO = "tavolinNo";
    private static final String ORARI = "orari";
    private static final String RESTORANT = "RestorantDto.

    private static final String VAKTI = "VaktiDto.
    private static final String PERSONELI = "PersoneliDto.

    @Query("select u from Rezervime u where (?1 is null OR UPPER (u.kodi) like '%'|| UPPER(?1) ||'%' ) "
            + "and ( ?2 is null OR UPPER (u.klientemer) like '%'|| UPPER(?2) ||'%' ) "
            + "and ( ?3 is null OR UPPER (u.tel1) like '%'|| UPPER(?3) ||'%' ) "
            + "and ( ?4 is null OR UPPER (u.kerkesaspecifike) like '%'|| UPPER(?4) ||'%' ) "
            + "and ( ?5 is null OR UPPER (u.tavolinNo) like '%'|| UPPER(?5) ||'%' ) "
            + "and ( ?6 is null OR UPPER (u.orari) like '%'|| UPPER(?6) ||'%' ) "
            + "and ( ?7 is null OR u.restorant.id= ?7 )"
            + "and ( ?7 is null OR u.restorant.emer=?8)"
            + "and ( ?8 is null OR u.vakti.id=?9)"
            + "and ( ?9 is null OR u.personeli.id= ?10 )")
    List<Personeli> searchRezervime(String kodi, String klientemer, String tel1, String kerkesaspecifike, Integer tavolinNo, String orari, String restorant, String vakti,
                                    String personeli, Pageable pageable);


    @Query("select count(u) from  Rezervime u where (?1 is null OR UPPER (u.kodi) like '%'|| UPPER(?1) ||'%' ) "
            + "and ( ?2 is null OR UPPER (u.klientemer) like '%'|| UPPER(?2) ||'%' ) "
            + "and ( ?3 is null OR UPPER (u.tel1) like '%'|| UPPER(?3) ||'%' ) "
            + "and ( ?4 is null OR UPPER (u.kerkesaspecifike) like '%'|| UPPER(?4) ||'%' ) "
            + "and ( ?5 is null OR UPPER (u.tavolinNo) like '%'|| UPPER(?5) ||'%' ) "
            + "and ( ?6 is null OR UPPER (u.orari) like '%'|| UPPER(?6) ||'%' ) "
            + "and ( ?7 is null OR u.restorant.id= ?7 )"
            + "and ( ?7 is null OR u.restorant.emer=?8)"
            + "and ( ?8 is null OR u.vakti.id=?9)"
            + "and ( ?9 is null OR u.personeli.id= ?10 )")
    Integer countRezervime(String kodi, String klientemer, String tel1, String kerkesaspecifike, Integer tavolinNo, String orari, String restorant, String vakti,
                           String personeli);
}
