package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.Turni;

public interface TurniRepository extends JpaRepository<Turni, Integer> {
}
