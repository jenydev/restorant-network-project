package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.TavolineType;

public interface TavolineTypeRepository extends JpaRepository<TavolineType, Integer> {
}
