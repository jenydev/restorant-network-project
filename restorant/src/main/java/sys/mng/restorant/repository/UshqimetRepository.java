package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.Ushqimet;

public interface UshqimetRepository extends JpaRepository<Ushqimet, Integer> {
}
