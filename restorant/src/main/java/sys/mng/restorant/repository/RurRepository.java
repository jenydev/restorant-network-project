package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.Rur;

public interface RurRepository extends JpaRepository<Rur, Integer> {
}
