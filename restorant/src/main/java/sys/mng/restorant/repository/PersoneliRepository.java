package sys.mng.restorant.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sys.mng.restorant.model.Personeli;

import java.util.List;

public interface PersoneliRepository extends JpaRepository<Personeli, Integer> {

    @Query("SELECT t FROM Personeli t WHERE t.turni.id = :Id")
    List<Personeli> selectPersoneliByTurniId (@Param("Id") Integer tId);

    @Query("select u from Personeli u where (?1 is null OR UPPER (u.emer) like '%'|| UPPER(?1) ||'%' ) "
            + "and ( ?2 is null OR UPPER (u.mbiemer) like '%'|| UPPER(?2) ||'%' ) "
            + "and ( ?3 is null OR UPPER (u.mosha) like '%'|| UPPER(?3) ||'%' ) "
            + "and ( ?4 is null OR UPPER (u.tel) like '%'|| UPPER(?4) ||'%' ) "
            + "and ( ?5 is null OR UPPER (u.email) like '%'|| UPPER(?5) ||'%' ) "
            + "and ( ?6 is null OR UPPER (u.adresa) like '%'|| UPPER(?6) ||'%' ) "
            + "and ( ?7 is null OR UPPER (u.paga) like '%'|| UPPER(?7) ||'%' ) "
            + "and ( ?8 is null OR UPPER (u.ditePushimi) like '%'|| UPPER(?8) ||'%' ) "
            + "and ( ?9 is null OR UPPER (u.muaj) like '%'|| UPPER(?9) ||'%' ) "
            + "and ( ?10 is null OR u.turni.id= ?10 )"
            + "and ( ?10 is null OR u.turni.turni=?11)"
            + "and ( ?10 is null OR u.turni.orari=?12)"
            + "and ( ?10 is null OR u.turni.id= ?13 )"
            + "and ( ?11 is null OR u.pozicioni.profesioni= ?14) ")
    List<Personeli> searchPersoneli(String emer, String mbiemer, String mosha, String tel, String email, String adresa, String paga, String ditePushimi,
                                    String muaj,String turni, String pozicioni, Pageable pageable);
//    String turni, String position,

    @Query("select count(u) from Personeli u where ( ?1 is null OR UPPER (u.emer) like '%'||UPPER(?1) ||'%' ) "
            + "and ( ?2 is null OR UPPER (u.mbiemer) like '%'|| UPPER(?2) ||'%' ) "
            + "and ( ?3 is null OR UPPER (u.mosha) like '%'|| UPPER(?3) ||'%' ) "
            + "and ( ?4 is null OR UPPER (u.tel) like '%'|| UPPER(?4) ||'%' ) "
            + "and ( ?5 is null OR UPPER (u.email) like '%'|| UPPER(?5) ||'%' ) "
            + "and ( ?6 is null OR UPPER (u.adresa) like '%'|| UPPER(?6) ||'%' ) "
            + "and ( ?7 is null OR UPPER (u.paga) like '%'|| UPPER(?7) ||'%' ) "
            + "and ( ?8 is null OR UPPER (u.ditePushimi) like '%'|| UPPER(?8) ||'%' ) "
            + "and ( ?9 is null OR UPPER (u.muaj) like '%'|| UPPER(?9) ||'%' ) "
            + "and ( ?10 is null OR u.turni.id= ?10 )"
            + "and ( ?10 is null OR u.turni.turni=?11)"
            + "and ( ?10 is null OR u.turni.orari=?12)"
            + "and ( ?10 is null OR u.turni.id= ?13 )"
            + "and ( ?11 is null OR u.pozicioni.profesioni= ?14) ")
    Integer countPersoneli(String emer, String mbiemer, String mosha, String tel, String email, String adresa, String paga, String ditePushimi,
                     String muaj,String turni, String pozicioni);
//    String turni, String position



}
