package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.Pozicioni;

public interface PozicioniRepository extends JpaRepository<Pozicioni, Integer> {
}
