package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.Menu;

public interface MenuRepository extends JpaRepository<Menu, Integer> {

}
