package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sys.mng.restorant.model.Tavolina;

public interface TavolinaRepository extends JpaRepository<Tavolina, Integer> {
}
