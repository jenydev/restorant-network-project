package sys.mng.restorant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sys.mng.restorant.model.Vakti;

@Repository
public interface VaktiRepository extends JpaRepository<Vakti, Integer> {
}
