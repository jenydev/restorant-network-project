package sys.mng.restorant.model;

import lombok.*;

import javax.persistence.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "rur")
public class Rur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @ManyToOne
    @JoinColumn(name = "rezervime_id", nullable = false)
    public Rezervime rezervime;

    @ManyToOne
    @JoinColumn(name = "ushqimet_id", nullable = false)
    public Ushqimet ushqimet;

    @ManyToOne
    @JoinColumn(name = "restorant_id", nullable = false)
    public Restorant restorant;

    @Column(name = "code_id", nullable = false)
    private String codeId;

    @Column(name = "sasia", nullable = false)
    private Integer sasia;
}
