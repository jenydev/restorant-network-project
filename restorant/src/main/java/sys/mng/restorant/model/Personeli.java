package sys.mng.restorant.model;

import lombok.*;

import javax.persistence.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "personeli")
public class Personeli {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "emer", nullable = false)
    private String emer;

    @Column(name = "mbiemer", nullable = false)
    private String mbiemer;

    @Column(name = "mosha", nullable = false)
    private Integer mosha;

    @Column(name = "gjinia", nullable = false)
    private String gjinia;

    @Column(name = "tel", nullable = false)
    private String tel;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "adresa", nullable = false)
    private String adresa;

    @Column(name = "paga", nullable = false)
    private Integer paga;

    @Column(name = "ditepushimi", nullable = false)
    private String ditePushimi;

    @Column(name = "muaj", nullable = false)
    private String muaj;

    @ManyToOne
    @JoinColumn(name = "turni_id", nullable = false)
    public Turni turni;

    @ManyToOne
    @JoinColumn(name = "pozicioni_id", nullable = false)
    public Pozicioni pozicioni;

}
