package sys.mng.restorant.model;

import lombok.*;

import javax.persistence.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "restorant")
public class Restorant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int restorantId;

    @Column(name = "emer", nullable = false)
    private String emer;
    @Column(name = "codeFilial", nullable = false)
    private String codeFilial;
    @Column(name = "nipt", nullable = false)
    private String nipt;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "qyteti", nullable = false)
    private String qyteti;
    @Column(name = "rruga", nullable = false)
    private String rruga;
    @Column(name = "tel_one", nullable = false)
    private String telOne;
    @Column(name = "tel_two", nullable = false)
    private String telTwo;

    @ManyToOne
    @JoinColumn(name = "tavolina_id", nullable = false)
    public Tavolina tavolina;

    @ManyToOne
    @JoinColumn(name = "tavoline_type_id", nullable = false)
    public TavolineType tavolineType;

    @ManyToOne
    @JoinColumn(name = "menu_id", nullable = false)
    public Menu menu;

    @ManyToOne
    @JoinColumn(name = "orar_operimi_id", nullable = false)
    public OrarOperimi orarOperimi;

    @ManyToOne
    @JoinColumn(name = "restorant_type_id", nullable = false)
    public RestorantType restorantType;


}
