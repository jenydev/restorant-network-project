package sys.mng.restorant.model;

import lombok.*;

import javax.persistence.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "rezervime")
public class Rezervime {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "kodi", nullable = false)
    private String kodi;
    @Column(name = "klientemer", nullable = false)
    private String klientEmer;
    @Column(name = "tel1", nullable = false)
    private String tel1;
    @Column(name = "kerkesaspecifike", nullable = false)
    private String kerkesaSpecifike;
    @ManyToOne
    @JoinColumn(name = "tavolin_no", nullable = false)
    public Integer tavolinNo;

    @Column(name = "orari", nullable = false)
    private String orari;

    @ManyToOne
    @JoinColumn(name = "restorant_id", nullable = false)
    public Restorant restorant;

    @ManyToOne
    @JoinColumn(name = "vakti_id", nullable = false)
    public Vakti vakti;

    @ManyToOne
    @JoinColumn(name = "personeli_id", nullable = false)
    public Personeli personeli;
}
