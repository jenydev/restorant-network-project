package sys.mng.restorant.model;

import lombok.*;

import javax.persistence.*;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "turni")
public class Turni {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "turni", nullable = false)
    private String turni;

    @Column(name = "orari", nullable = false)
    private String orari;
}
